
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.io.*,java.util.*" %>

<%
    boolean registerBool = true;
    String encodedRegisterBool = String.valueOf(registerBool);
%>
<!DOCTYPE html>
<html>
    <head>
        <title>GDTG</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/login.css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="login_form">
            <form action="login" method="POST" class="form">
                <h1 class="form_title" style="margin-bottom: 20px"> Log In </h1>
                <p>${logResult}</p>
                <div class="form_div" style="margin-top: 10px">
                    <input type="text" value="${username}"class="form_input" placeholder=" " name="username" required >
                    <label class="form_label">Username</label>
                </div>

                <div class="form_div">
                    <input type="password" value="${password}" class="form_input" placeholder=" " name="password" required>
                    <label class="form_label">Password</label>
                </div>
                
                <input type="checkbox" name="remember" value="ON" id="checkboxReme" checked/>
                <label for="checkboxReme">Remember me</label>
                    
                <div id="captcha" class="form_div">
                    <img src="${captchaImage}" alt="captcha"/>
                    <div class="captcha_form"> 
                        <input type="text" id="captcha_form" class="form_input_captcha" placeholder=" " name="captcha" required style="margin-top: 10px">
                        <label class="form_label_captcha">Enter Captcha</label>
                        <button type ="button" class="captcha_refersh" onclick="window.location.href='captcha'" style="margin-top: 10px">
                            <i class="fa fa-refresh"></i>
                        </button>
                    </div>
                </div>

                <input type="submit" class="form_button" value="Log In">
                <button type="button" class="form_button" onclick="redirectToCaptcha('<%= encodedRegisterBool %>')" style="margin-top: 10px; background-color: #3498db;">
                    <i class="fa fa-user-plus"></i> Register
                </button>

                <script>
                    function redirectToCaptcha(registerBool) {
                        // Chuyển đến trang mới
                        window.location.href = 'captcha?registerBool=' + encodeURIComponent(registerBool);
                    }
                </script>
            </form>
        </div>
        <script src="js/login.js"></script>
    </body>
</html>
