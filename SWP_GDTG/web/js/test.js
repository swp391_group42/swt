

function openForm() {
    document.getElementById("myForm").style.display = "block";
}

function closeForm() {
    document.getElementById("myForm").style.display = "none";
}
function validateNumber(input) {
    // Loại bỏ các ký tự không phải số từ giá trị đầu vào
    input.value = input.value.replace(/[^0-9.]/g, '');

    // Nếu muốn giới hạn số lượng số thập phân, bạn có thể sử dụng một biểu thức chính quy như /^(\d*\.\d{0,2}|\d*)$/
}