package controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.imageio.ImageIO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.UUID;

import nl.captcha.Captcha;
import nl.captcha.backgrounds.GradiatedBackgroundProducer;
import nl.captcha.gimpy.DropShadowGimpyRenderer;
import nl.captcha.text.renderer.ColoredEdgesWordRenderer;

import redis.clients.jedis.Jedis;

public class CaptchaServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        Captcha captcha = null;
        Jedis jedis = null;
        String key = (String)request.getSession().getAttribute("currentKey");
        String oldKey = key;
        try {
            jedis = new Jedis("redis-11452.c252.ap-southeast-1-1.ec2.cloud.redislabs.com", 11452);
            jedis.auth("ZDlwKg44kyYu3YqiJMMDnLxGzsogBMg4");

            if (oldKey != null ) {
                jedis.del(oldKey);
                key = generateUniqueCaptchaId(jedis); 
                request.getSession().setAttribute("currentKey",key);
            } else {
                key = generateUniqueCaptchaId(jedis); 
                request.getSession().setAttribute("currentKey",key);
            }

            // Tạo CAPTCHA
            captcha = new Captcha.Builder(200, 50)
                    .addText(new ColoredEdgesWordRenderer())
                    .addNoise()
                    .addBackground(new GradiatedBackgroundProducer())
                    .gimp(new DropShadowGimpyRenderer())
                    .addBorder()
                    .build();
            // Lưu vào redis
            jedis.setex(key, 100, captcha.getAnswer());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            jedis.close();
        }

        // Chuyển đổi BufferedImage thành dữ liệu Base64
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(captcha.getImage(), "png", baos);
        byte[] imageData = Base64.getEncoder().encode(baos.toByteArray());
        String imageDataString = new String(imageData, StandardCharsets.UTF_8);

        // Gửi dữ liệu
        request.setAttribute("captchaImage", "data:image/png;base64," + imageDataString);
        request.setAttribute("logResult", request.getParameter("logResult"));

        
        // Forward đến trang khác
        Boolean registerBool = Boolean.parseBoolean(request.getParameter("registerBool"));
        request.setAttribute("registerBool", registerBool);
        if(registerBool==true){
            request.getRequestDispatcher("register.jsp").forward(request, response);
        }else{
            request.getRequestDispatcher("login").forward(request, response);
        }
    }
    
    private String generateUniqueCaptchaId(Jedis jedis) {
        String captchaId;
        do {
            captchaId = UUID.randomUUID().toString();
        } while (jedis.exists("captcha:" + captchaId));

        return captchaId;
    }

    
}
