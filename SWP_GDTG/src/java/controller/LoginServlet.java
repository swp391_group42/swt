/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.ArrayList;

import  model.*;
import redis.clients.jedis.Jedis;

public class LoginServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Cookie[] cookies = request.getCookies();
        String username = null;
        String password = null;

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("usernameCk")) {
                    username = cookie.getValue();
                } else if (cookie.getName().equals("passwordCk")) {
                    password = cookie.getValue();
                }
            }
        }
        if(username != null && password != null){
            request.setAttribute("username", username);
            request.setAttribute("password", password);
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String username = (String) request.getParameter("username");
        String password = (String) request.getParameter("password");
        String captcha = (String) request.getParameter("captcha");
        String remember = (String) request.getParameter("remember");
        String captchaString = "";
        
        UserDAO userDAO = new UserDAO();
        User currentUser = userDAO.getUserByUsername(username);
        
        
        String logResult;
        try {
            Jedis jedis = new Jedis("redis-11452.c252.ap-southeast-1-1.ec2.cloud.redislabs.com", 11452);
            jedis.auth("ZDlwKg44kyYu3YqiJMMDnLxGzsogBMg4");
            String currentKey = (String)request.getSession().getAttribute("currentKey");
            String value = jedis.get(currentKey);
            jedis.close();
            if(value == null){
                logResult = "Captcha time out";
                request.setAttribute("logResult", logResult);
                response.sendRedirect("captcha?logResult=" + URLEncoder.encode(logResult, "UTF-8"));
            }else if(!value.equals(captcha)){
                logResult = "Wrong Captcha";
                request.setAttribute("logResult", logResult);
                response.sendRedirect("captcha?logResult=" + URLEncoder.encode(logResult, "UTF-8"));
            }else{
                if (currentUser == null) {
                    logResult = "Username not exist";
                    request.setAttribute("logResult", logResult);
                    response.sendRedirect("captcha?logResult=" + URLEncoder.encode(logResult, "UTF-8"));
                } else if (!currentUser.getPassword().equals(password)) {
                    logResult = "Wrong password";
                    request.setAttribute("logResult", logResult);
                    response.sendRedirect("captcha?logResult=" + URLEncoder.encode(logResult, "UTF-8"));

                } else {
                    request.getSession().setAttribute("currentUser", currentUser);
                    Cookie u = new Cookie("usernameCk", username);
                    Cookie p = new Cookie("passwordCk", password);
                    if(remember != null){
                        p.setMaxAge(60*60*24);
                        u.setMaxAge(60*60*24);
                    }else{
                        p.setMaxAge(0);
                        u.setMaxAge(0);
                    }
                    response.addCookie(p);
                    response.addCookie(u);
                    request.getRequestDispatcher("home.jsp").forward(request, response);
                }
            }
        } catch (Exception e) {
        }

        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
