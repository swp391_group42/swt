/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class OrderDAO {
//    public void saveOrder(Order order) {
//        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
//            Transaction transaction = session.beginTransaction();
//            session.save(order);
//            transaction.commit();
//            
//            HibernateUtil.closeSessionFactory();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    
    public void saveOrder(Order order) {
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            session.save(order);
            session.getTransaction().commit();
            
            HibernateUtil.closeSessionFactory();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Add other methods for CRUD operations
    public List<Order> getAllOrders() {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // HQL query to select all orders
            String hql = "FROM Order";

            // Create a Query to execute the query
            Query<Order> query = session.createQuery(hql, Order.class);

            // Execute the query and return the result
            return query.list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public Order getOrderById(String orderCode) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            return session.get(Order.class, orderCode);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void updateOrder(Order order) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(order);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteOrder(Order order) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.delete(order);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

