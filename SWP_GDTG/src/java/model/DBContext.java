package model;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBContext {
 
    private static Connection con;
    private static String URL;
    private static String USER;
    private static String PASSWORD;

    static {
        try {
            // Load the MySQL JDBC driver
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 
    public static Connection getConnection() {
    con = null;
    Properties properties = new Properties();
    try (FileReader reader = new FileReader(new File("info.properties"))) {
        properties.load(reader);
        System.out.println("File path: " + new File("info.properties").getAbsolutePath());
        URL = properties.getProperty("url");
        USER = properties.getProperty("user");
        PASSWORD = properties.getProperty("password");
        con = DriverManager.getConnection(URL, USER, PASSWORD);
    } catch (IOException | SQLException ex) {
        Logger.getLogger(DBContext.class.getName()).log(Level.SEVERE, null, ex);
    }
    return con;
}

 
    public static void freeConnection() {
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
