/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "User")
public class User {
    @Id
    @Column(name = "UserID")
    private Integer userID;

    @Column(name = "Username")
    private String username;

    @Column(name = "Password")
    private String password;

    @Column(name = "Phone")
    private String phone;

    @Column(name = "Email")
    private String email;

    @Lob
    @Column(name = "MoreInfor")
    private String moreInfo;

    @Column(name = "Balance")
    private Double balance;

    @Enumerated(EnumType.STRING)
    private Role role;

    @Temporal(TemporalType.DATE)
    private Date createdAt;

    @Column(name = "IsDeleted")
    private Boolean isDeleted;
    
    //Getter and Setter
    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMoreInfo() {
        return moreInfo;
    }

    public void setMoreInfo(String moreInfo) {
        this.moreInfo = moreInfo;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    //Constructor

    public User() {
    }

    public User(Integer userID, String username, String password, String phone, String email, String moreInfo, Double balance, Role role, Date createdAt, Boolean isDeleted) {
        this.userID = userID;
        this.username = username;
        this.password = password;
        this.phone = phone;
        this.email = email;
        this.moreInfo = moreInfo;
        this.balance = balance;
        this.role = role;
        this.createdAt = createdAt;
        this.isDeleted = isDeleted;
    }
    //ENUM
    public enum Role {
        Admin,
        User
    }
}

