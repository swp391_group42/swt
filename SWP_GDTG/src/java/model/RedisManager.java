package model;

import redis.clients.jedis.Jedis;

public class RedisManager {

    private static final String REDIS_HOST = "redis-11452.c252.ap-southeast-1-1.ec2.cloud.redislabs.com";
    private static final int REDIS_PORT = 11452;
    private static final String REDIS_PASSWORD = "ZDlwKg44kyYu3YqiJMMDnLxGzsogBMg4";

    private static Jedis jedis;

    // Đảm bảo rằng chỉ có một đối tượng được tạo ra
    private static RedisManager instance;

    private RedisManager() {
        // Khởi tạo kết nối Redis ở đây
        jedis = new Jedis(REDIS_HOST, REDIS_PORT);
        jedis.auth(REDIS_PASSWORD);
    }

    public static synchronized RedisManager getInstance() {
        if (instance == null) {
            instance = new RedisManager();
        }
        return instance;
    }

    public Jedis getConnection() {
        return jedis;
    }

    public void closeConnection() {
        jedis.close();
    }
}
   

