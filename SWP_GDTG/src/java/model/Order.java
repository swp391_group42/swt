/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Order")
public class Order {
    @Id
    @Column(name = "OrderCode")
    private String orderCode;

    @ManyToOne
    @JoinColumn(name = "SellerID", referencedColumnName = "UserID")
    private User seller;

    @ManyToOne
    @JoinColumn(name = "BuyerID", referencedColumnName = "UserID")
    private User buyer;

    @Enumerated(EnumType.STRING)
    private OrderState state;

    private String title;

    @Lob
    private String content;

    @Lob
    @Column(name = "HideContent")
    private String hideContent;

    private Float unitPrice;

    @Temporal(TemporalType.DATE)
    private Date createdAt;

    @ManyToOne
    @JoinColumn(name = "CreatedBy", referencedColumnName = "UserID")
    private User createdBy;

    @Temporal(TemporalType.DATE)
    private Date lastUpdate;

    private Boolean isDeleted;

    @Enumerated(EnumType.STRING)
    private FeeRole fee;

    // Getters and setters
    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public User getBuyer() {
        return buyer;
    }

    public void setBuyer(User buyer) {
        this.buyer = buyer;
    }

    public OrderState getState() {
        return state;
    }

    public void setState(OrderState state) {
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getHideContent() {
        return hideContent;
    }

    public void setHideContent(String hideContent) {
        this.hideContent = hideContent;
    }

    public Float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public FeeRole getFee() {
        return fee;
    }

    public void setFee(FeeRole fee) {
        this.fee = fee;
    }

    // Constructors

    public Order() {
    }

    public Order(String orderCode, User seller, User buyer, OrderState state, String title, String content, String hideContent, Float unitPrice, Date createdAt, User createdBy, Date lastUpdate, Boolean isDeleted, FeeRole fee) {
        this.orderCode = orderCode;
        this.seller = seller != null ? seller : null;
        this.buyer = buyer != buyer ? buyer : null;
        this.state = state;
        this.title = title;
        this.content = content;
        this.hideContent = hideContent;
        this.unitPrice = unitPrice;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.lastUpdate = lastUpdate;
        this.isDeleted = isDeleted;
        this.fee = fee;
    }
    
    public enum FeeRole {
        Seller,
        Buyer
    }
    public enum OrderState {
        Pending,
        Processing,
        Completed,
        Cancelled
    }

}

