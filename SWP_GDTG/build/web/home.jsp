<%-- 
    Document   : home
    Created on : Jan 19, 2024, 5:21:38 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!--=============== REMIXICONS ===============-->
        <link href="https://cdn.jsdelivr.net/npm/remixicon@3.2.0/fonts/remixicon.css" rel="stylesheet">

        <!--=============== CSS ===============-->


        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="css/form.css" rel="stylesheet" type="text/css"/>
        <link href="css/table.css" rel="stylesheet" type="text/css"/>

        <title>GDTG</title>
    </head>
    <body>
        <!--=============== HEADER ===============-->
        <header class="header">
            <nav class="nav container nav-container">
                <div class="nav__data">
                    <a href="#" class="nav__logo">
                        <i class="ri-planet-line"></i> NHOM4
                    </a>

                    <div class="nav__toggle" id="nav-toggle">
                        <i class="ri-menu-line nav__burger"></i>
                        <i class="ri-close-line nav__close"></i>
                    </div>
                </div>

                <!--=============== NAV MENU ===============-->
                <div class="nav__menu" id="nav-menu">
                    <ul class="nav__list">
                        <li><a href="#" class="nav__link">Trang chủ</a></li>



                        <!--=============== DROPDOWN 1 ===============-->
                        <li class="dropdown__item">
                            <div class="nav__link">
                                Trung gian <i class="ri-arrow-down-s-line dropdown__arrow"></i>
                            </div>

                            <ul class="dropdown__menu">
                                <li>
                                    <a href="#" class="dropdown__link">
                                        <i class="ri-pie-chart-line"></i> abc
                                    </a>                          
                                </li>

                                <li>
                                    <a href="#" class="dropdown__link">
                                        <i class="ri-arrow-up-down-line"></i> abc
                                    </a>
                                </li>

                                <!--=============== DROPDOWN SUBMENU ===============-->
                                <li class="dropdown__subitem">
                                    <div class="dropdown__link">
                                        <i class="ri-bar-chart-line"></i> abc <i class="ri-add-line dropdown__add"></i>
                                    </div>

                                    <ul class="dropdown__submenu">
                                        <li>
                                            <a href="#" class="dropdown__sublink">
                                                <i class="ri-file-list-line"></i> abc
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#" class="dropdown__sublink">
                                                <i class="ri-cash-line"></i> abc
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#" class="dropdown__sublink">
                                                <i class="ri-refund-2-line"></i> abc
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <!--=============== DROPDOWN 2 ===============-->
                        <li class="dropdown__item">
                            <div class="nav__link">
                                Mua hàng <i class="ri-arrow-down-s-line dropdown__arrow"></i>
                            </div>

                            <ul class="dropdown__menu">
                                <li>
                                    <a href="#" class="dropdown__link">
                                        <i class="ri-user-line"></i> abc
                                    </a>                          
                                </li>

                                <li>
                                    <a href="#" class="dropdown__link">
                                        <i class="ri-lock-line"></i> abc
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="dropdown__link">
                                        <i class="ri-message-3-line"></i> abc
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!--=============== DROPDOWN 3 ===============-->
                        <li class="dropdown__item">
                            <div class="nav__link">
                                Thanh toán <i class="ri-arrow-down-s-line dropdown__arrow"></i>
                            </div>

                            <ul class="dropdown__menu">
                                <li>
                                    <a href="#" class="dropdown__link">
                                        <i class="ri-user-line"></i> abc
                                    </a>                          
                                </li>

                                <li>
                                    <a href="#" class="dropdown__link">
                                        <i class="ri-lock-line"></i> abc
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="dropdown__link">
                                        <i class="ri-message-3-line"></i> abc
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!--=============== DROPDOWN 4 ===============-->
                        <li class="dropdown__item">
                            <div class="nav__link">
                                Thông tin <i class="ri-arrow-down-s-line dropdown__arrow"></i>
                            </div>

                            <ul class="dropdown__menu">
                                <li>
                                    <a href="#" class="dropdown__link">
                                        <i class="ri-user-line"></i> Profiles
                                    </a>                          
                                </li>

                                <li>
                                    <a href="#" class="dropdown__link">
                                        <i class="ri-lock-line"></i> Accounts
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="dropdown__link">
                                        <i class="ri-message-3-line"></i> Messages
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <div style="height: 10000px;
             margin-top: 100px;
             " class="app-body">
            <div class="mt-4 container-fluid table-container">
                <marquee behavior="scroll" direction="left"><h6 style="color: red;">abcxyz</h6></marquee>

                <div class="col-md-12">
                    <div class="card-group">
                        <div class="card">
                            <div class="card-header">
                                <div class="left-item">
                                    <h3 class="text mb-0">
                                        Đơn công khai
                                    </h3>

                                </div>

                                <div class="right-item">
                                    <button onclick="openForm()">Tạo đơn</button>
                                </div>
                            </div>

                            <div class="card-body">
                                <!--                        <div style="overflow-x: auto;">
                                                           <table>
                                                             <tr>
                                                               <th>Mã đơn hàng</th>
                                                               <th>Trạng thái</th>
                                                               <th>Người mua</th>
                                                               <th>Chủ đề</th>
                                                               <th>Công khai/Riêng tư</th>
                                                               <th>Giá tiền</th>
                                                               <th>Bên chịu phí</th>
                                                               <th>Phí giao dịch</th>
                                                               <th>Tiền thực nhận</th>
                                                               <th>Thời gian tạo</th>
                                                         
                                                             </tr>
                                                             <tr>
                                                               <td>001</td>
                                                               <td>COMPLETE</td>
                                                               <td>001</td>
                                                               <td>Account gunny</td>
                                                               <td>Công khai</td>
                                                               <td>50</td>
                                                               <td>Người mua</td>
                                                               <td>5</td>
                                                               <td>50</td>
                                                               <td>24/01/2024</td>
                                                         
                                                             </tr>
                                                             <tr>
                                                               <td>002</td>
                                                               <td>COMPLETE</td>
                                                               <td>005</td>
                                                               <td>Netflix</td>
                                                               <td>Công khai</td>
                                                               <td>94</td>
                                                               <td>Người bán</td>
                                                               <td>9.4</td>
                                                               <td>84,6</td>
                                                               <td>24/01/2024</td>
                                                         
                                                             </tr>
                                                             <tr>
                                                               <td>003</td>
                                                               <td>COMPLETE</td>
                                                               <td>002</td>
                                                               <td>Facebook</td>
                                                               <td>Công khai</td>
                                                               <td>67</td>
                                                               <td>Người mua</td>
                                                               <td>6.7</td>
                                                               <td>6.7</td>
                                                               <td>24/01/2024</td>
                                                         
                                                             </tr>
                                                           </table>
                                                         </div>
                                                     </div>-->
                                <div style="overflow-x: auto;">
                                    <table>
                                        <tr>
                                            <th>Mã đơn hàng</th>
                                            <th>Trạng thái</th>
                                            <th>Người mua</th>
                                            <th>Chủ đề</th>
                                            <th>Công khai/Riêng tư</th>
                                            <th>Giá tiền</th>
                                            <th>Bên chịu phí</th>
                                            <th>Phí giao dịch</th>
                                            <th>Tiền thực nhận</th>
                                            <th>Thời gian tạo</th>

                                        </tr>
                                        <!-- Check if orderList is available in the request -->
                                        <c:if test="${not empty orderList}">
                                            <!-- Use JSTL to iterate over the orderList and generate table rows -->
                                            <c:forEach var="order" items="${orderList}">
                                                <tr>
                                                    <td>${order.orderCode}</td>
                                                    <!-- Add other fields here -->
                                                </tr>
                                            </c:forEach>
                                        </c:if>

                                        <!-- If orderList is empty or not available, display a message -->
                                        <c:if test="${empty orderList}">
                                            <tr>
                                                <td colspan="10">No orders available</td>
                                            </tr>
                                        </c:if>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="container form-container" id="myForm">
                <form action="/SWP_GDTG/CreateOrder" method="POST">
                    <div class="row">
                        <div class="col-25">
                            <label for="topic">Chủ đề chung gian</label>
                        </div>
                        <div class="col-75">
                            <input type="text" id="topic" name="topic" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="price">Giá tiền</label>
                        </div>
                        <div class="col-75">
                            <input type="text" id="price" name="price" placeholder="Your price.." oninput="validateNumber(this)">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="fee_role">Bên chịu phí trung gian</label>
                        </div>
                        <div class="col-75">
                            <select id="fee_role" name="fee_role">
                                <option value="" selected disabled>Select an option</option>
                                <option value="seller">Bên bán</option>
                                <option value="buyer">Bên mua</option>

                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="description">Mô tả</label>
                        </div>
                        <div class="col-75">
                            <textarea id="description" name="description" placeholder="Write something.." style="height:200px"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="hidden_content">Thông tin ẩn</label>
                        </div>
                        <div class="col-75">
                            <textarea id="hidden_content" name="hidden_content" placeholder="Write something.." style="height:200px"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-25">
                            <label for="show_option">Hiển thị</label>
                        </div>
                        <div class="col-75">
                            <select id="show_option" name="show_option">
                                <option value="" selected disabled>Select an option</option>
                                <option value="1">Công khai</option>
                                <option value="0">Riêng tư</option>

                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <input type="submit" value="Submit">
                        <button type="button" class="btn" onclick="closeForm()">Close</button>
                    </div>
                </form>
            </div>

        </div>


        <footer class="app-footer">

        </footer>

        <!--=============== MAIN JS ===============-->
        <script src="js/main.js" type="text/javascript"></script>
        <script src="js/test.js" type="text/javascript"></script>
    </body>
</html>
