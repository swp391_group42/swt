<%-- 
    Document   : register
    Created on : Jan 23, 2024, 12:47:31 AM
    Author     : DELL
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
            <form action="register" method="POST" >
                <h1> Register </h1>
                <p>${regisResult}</p>
                <p><input type="text"  placeholder=" " name="username" required >
                    <label>Username</label></p>
                    

                <p><input type="password" placeholder=" " name="password" required>
                    <label>Password</label></p>
                    
                <p><label for="largeInput">Flex bản thân:</label><br>
                    <textarea id="largeInput" name="largeInput" rows="4" cols="50"></textarea><br><br></p>    
                    

                    
                <div id="captcha" class="form_div">
                    <img src="${captchaImage}" alt="captcha"/>
                    <div class="captcha_form"> 
                        <input type="text" id="captcha_form" class="form_input_captcha" placeholder=" " name="captcha" required style="margin-top: 10px">
                        <label class="form_label_captcha">Enter Captcha</label>
                        <button type ="button" class="captcha_refersh" onclick="window.location.href='captcha'" style="margin-top: 10px">
                            <i class="fa fa-refresh"></i>
                        </button>
                    </div>
                </div>

                <input type="submit" class="form_button" value="Register">
                <button type="button" onclick="window.location.href='captcha'" style="margin-top: 10px; background-color:  #3498db;">
                    <i class="fa fa-user"></i>  Login
                </button>
            </form>
    </body>
</html>
